plugins {
    kotlin("jvm") version "1.4.10"
    application
}

repositories {
    mavenCentral()
    jcenter()
}

dependencies {
    implementation("com.serebit.strife", "strife-client", "0.4.0")
}

application.mainClass.set("MainKt")
