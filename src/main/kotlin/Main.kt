import com.serebit.strife.bot
import com.serebit.strife.entities.reply
import com.serebit.strife.onMessageCreate
import com.serebit.strife.onReady

// uses the first arg passed to the function as the token. Remember, don't hardcode your tokens!
suspend fun main(args: Array<String>) = bot(token = args[0]) {
    // Prints a message to the console once the bot is fully connected to Discord.
    onReady { println("Connected to Discord!") }

    // When a message is received, if the message content is "!ping", send a message in response that says "Pong!"
    onMessageCreate {
        if (message.getContent() == "!ping") message.reply("Pong!")
    }
}
